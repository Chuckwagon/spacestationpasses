package com.charliechristensen.spacestation.spacestationpasses.util

import com.charliechristensen.spacestation.spacestationpasses.util.CoordinateUtil.MAX_LATITUDE
import com.charliechristensen.spacestation.spacestationpasses.util.CoordinateUtil.MAX_LONGITUDE
import org.junit.Test

/**
 * Tests Utility functions which verify valid coordinates
 */
class CoordinateUtilTest {

    /**
     * Although this relies on calling the other methods, testing this separately
     * will protect against any changes to the implementation
     */
    @Test
    fun areValidCoordinates() {
        for(latitude in -MAX_LATITUDE.toInt()..MAX_LATITUDE.toInt()){
            assert(CoordinateUtil.areValidCoordinates(latitude.toDouble(), MAX_LONGITUDE))
        }
        for(longitude in -MAX_LONGITUDE.toInt()..MAX_LONGITUDE.toInt()){
            assert(CoordinateUtil.areValidCoordinates(MAX_LATITUDE, longitude.toDouble()))
        }
        assert(CoordinateUtil.areValidCoordinates(MAX_LATITUDE + 1, MAX_LONGITUDE))
        assert(CoordinateUtil.areValidCoordinates(-MAX_LATITUDE - 1, MAX_LONGITUDE))
        assert(CoordinateUtil.areValidCoordinates(MAX_LATITUDE, MAX_LONGITUDE + 1))
        assert(CoordinateUtil.areValidCoordinates(MAX_LATITUDE, -MAX_LONGITUDE - 1))
    }

    @Test
    fun isLatitudeValid() {
        for(latitude in -MAX_LATITUDE.toInt()..MAX_LATITUDE.toInt()){
            assert(CoordinateUtil.isLatitudeValid(latitude.toDouble()))
        }
        assert(CoordinateUtil.isLatitudeValid(MAX_LATITUDE + 1))
        assert(CoordinateUtil.isLatitudeValid(-MAX_LATITUDE - 1))
    }

    @Test
    fun isLongitudeValid() {
        for(longitude in -MAX_LONGITUDE.toInt()..MAX_LONGITUDE.toInt()){
            assert(CoordinateUtil.isLongitudeValid(longitude.toDouble()))
        }
        assert(CoordinateUtil.isLongitudeValid(MAX_LONGITUDE + 1))
        assert(CoordinateUtil.isLongitudeValid(-MAX_LONGITUDE - 1))
    }

}