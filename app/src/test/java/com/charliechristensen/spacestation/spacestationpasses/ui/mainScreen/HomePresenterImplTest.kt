package com.charliechristensen.spacestation.spacestationpasses.ui.mainScreen

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test
import org.mockito.ArgumentMatchers.anyDouble

/**
 * Unit tests for HomePresenterImpl
 */
class HomePresenterImplTest {

    @Test
    @Throws(Exception::class)
    fun getStationPassesWithCoordinatesButtonClick() {
        val homePresenter = HomePresenterImpl()

        val successView : HomeView = mock()
        homePresenter.attachView(successView)
        homePresenter.getStationPassesWithCoordinatesButtonClick("10.0", "10.0")
        verify(successView).hideLatitudeError()
        verify(successView).hideLongitudeError()
        verify(successView).pushStationPassListActivity(10.0, 10.0)
        homePresenter.detachView()

        val latitudeOutOfRangeView : HomeView = mock()
        homePresenter.attachView(latitudeOutOfRangeView)
        homePresenter.getStationPassesWithCoordinatesButtonClick("91.0", "10.0")
        verify(latitudeOutOfRangeView).showLatitudeError()
        verify(latitudeOutOfRangeView).hideLongitudeError()
        verify(latitudeOutOfRangeView, never()).pushStationPassListActivity(anyDouble(), anyDouble())
        homePresenter.detachView()

        val longitudeOutOfRangeView : HomeView = mock()
        homePresenter.attachView(longitudeOutOfRangeView)
        homePresenter.getStationPassesWithCoordinatesButtonClick("10.0", "181.0")
        verify(longitudeOutOfRangeView).hideLatitudeError()
        verify(longitudeOutOfRangeView).showLongitudeError()
        verify(longitudeOutOfRangeView, never()).pushStationPassListActivity(anyDouble(), anyDouble())
        homePresenter.detachView()

        val improperFormattedView : HomeView = mock()
        homePresenter.attachView(improperFormattedView)
        homePresenter.getStationPassesWithCoordinatesButtonClick("fail", "fail")
        verify(improperFormattedView).showLatitudeError()
        verify(improperFormattedView).showLongitudeError()
        verify(longitudeOutOfRangeView, never()).pushStationPassListActivity(anyDouble(), anyDouble())
        homePresenter.detachView()
    }

    @Test
    @Throws(Exception::class)
    fun getStationPassesWithCurrentLocationButtonClick() {
        val homePresenter = HomePresenterImpl()
        val hasPermissionsView : HomeView = mock{
            on { hasLocationPermissions } doReturn true
        }
        homePresenter.attachView(hasPermissionsView)
        homePresenter.getStationPassesWithCurrentLocationButtonClick()
        verify(hasPermissionsView).pushStationPassListActivityWithLocationMonitoring()
        homePresenter.detachView()

        val noPermissionsView : HomeView = mock{
            on { hasLocationPermissions } doReturn false
        }
        homePresenter.attachView(noPermissionsView)
        homePresenter.getStationPassesWithCurrentLocationButtonClick()
        verify(noPermissionsView).requestLocationPermissions()
    }

    @Test
    @Throws(Exception::class)
    fun testLocationPermissionsGranted() {
        val homePresenter = HomePresenterImpl()
        val view : HomeView = mock()
        homePresenter.attachView(view)
        homePresenter.locationPermissionsGranted()
        verify(view).pushStationPassListActivityWithLocationMonitoring()
    }

    @Test
    @Throws(Exception::class)
    fun testLocationPermissionDenied() {
        val homePresenter = HomePresenterImpl()
        val view : HomeView = mock()
        homePresenter.attachView(view)
        homePresenter.locationPermissionDenied()
        verify(view).showPermissionDeniedError()
    }

}