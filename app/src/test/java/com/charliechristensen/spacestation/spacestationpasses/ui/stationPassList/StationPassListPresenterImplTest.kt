package com.charliechristensen.spacestation.spacestationpasses.ui.stationPassList

import com.charliechristensen.spacestation.spacestationpasses.data.models.PassInfo
import com.charliechristensen.spacestation.spacestationpasses.data.repository.Repository
import com.charliechristensen.spacestation.spacestationpasses.util.scheduler.RxScheduler
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Test
import java.io.IOException

/**
 * Unit tests for StationPassListPresenterImpl.
 *
 * INCOMPLETE
 *
 */
class StationPassListPresenterImplTest {

    private val validCoordinate = 90.0
    private val invalidCoordinate = 181.0
    private val errorCoordinate = 70.0
    private val networkErrorCoordinate = 60.0

    private val mockPassInfo : PassInfo = mock()

    private val mockRepository : Repository = mock{
        on { getPassTimesForCoordinates(validCoordinate, validCoordinate, 25) } doReturn Single.just(listOf(mockPassInfo))
        on { getPassTimesForCoordinates(errorCoordinate, errorCoordinate, 25) } doReturn Single.error(Throwable())
        on { getPassTimesForCoordinates(networkErrorCoordinate, networkErrorCoordinate, 25) } doReturn Single.error(IOException())
    }
    private val mockScheduler : RxScheduler = mock{
        on { mainThread() } doReturn Schedulers.trampoline()
        on { backgroundThread() } doReturn Schedulers.trampoline()
    }

    @Test
    fun onViewBound() {
        //TODO
    }

    @Test
    fun testDetachViewMonitorLocation() {
        val presenter = StationPassListPresenterImpl(mockRepository, mockScheduler, true)
        val view : StationPassListView = mock()
        presenter.attachView(view)
        presenter.detachView()
        verify(view).stopMonitoringLocation()
    }

    @Test
    fun testDetachViewWithoutMonitorLocation() {
        val presenter = StationPassListPresenterImpl(mockRepository, mockScheduler, false)
        val view : StationPassListView = mock()
        presenter.attachView(view)
        presenter.detachView()
        verify(view, never()).stopMonitoringLocation()
    }

    @Test
    fun refreshList() {
        //TODO
    }

    @Test
    fun testFetchStationPassesWithValidCoordinates() {
        val presenter = StationPassListPresenterImpl(mockRepository, mockScheduler, true)
        val view : StationPassListView = mock()
        presenter.attachView(view)
        presenter.fetchStationPasses(validCoordinate, validCoordinate, true)

        assert(presenter.objects.size == 1)
        assert(presenter.objects[0] == mockPassInfo)

        verify(view).setRefreshing(true)
        verify(view).setRefreshing(false)
        verify(view).setNoResultsTextViewVisible(false)
        verify(view).addItems(listOf(mockPassInfo), true)
    }

    @Test
    fun testFetchStationPassesWithInvalidLatitude() {
        val presenter = StationPassListPresenterImpl(mockRepository, mockScheduler, true)
        val view : StationPassListView = mock()
        presenter.attachView(view)
        presenter.fetchStationPasses(invalidCoordinate, validCoordinate, true)

        assert(presenter.objects.isEmpty())

        verify(view).setRefreshing(false)
        verify(view).setNoResultsTextViewVisible(true)
        verify(view).showInvalidCoordinatesError()
    }

    @Test
    fun testFetchStationPassesWithInvalidLongitude() {
        val presenter = StationPassListPresenterImpl(mockRepository, mockScheduler, true)
        val view : StationPassListView = mock()
        presenter.attachView(view)
        presenter.fetchStationPasses(validCoordinate, invalidCoordinate, true)

        assert(presenter.objects.isEmpty())

        verify(view).setRefreshing(false)
        verify(view).setNoResultsTextViewVisible(true)
        verify(view).showInvalidCoordinatesError()
    }

    @Test
    fun testFetchStationPassesWithError() {
        val presenter = StationPassListPresenterImpl(mockRepository, mockScheduler, true)
        val view : StationPassListView = mock()
        presenter.attachView(view)
        presenter.fetchStationPasses(errorCoordinate, errorCoordinate, true)

        assert(presenter.objects.isEmpty())

        verify(view).setRefreshing(true)
        verify(view).setRefreshing(false)
        verify(view).setNoResultsTextViewVisible(true)
        verify(view).showUnknownError()
    }

    @Test
    fun testFetchStationPassesWithNetworkError() {
        val presenter = StationPassListPresenterImpl(mockRepository, mockScheduler, true)
        val view : StationPassListView = mock()
        presenter.attachView(view)
        presenter.fetchStationPasses(networkErrorCoordinate, networkErrorCoordinate, true)

        assert(presenter.objects.isEmpty())

        verify(view).setRefreshing(true)
        verify(view).setRefreshing(false)
        verify(view).setNoResultsTextViewVisible(true)
        verify(view).showNetworkError()
    }

    @Test
    fun testNoProvidersEnabledError() {
        val presenter = StationPassListPresenterImpl(mockRepository, mockScheduler, true)
        val view : StationPassListView = mock()
        presenter.attachView(view)
        presenter.noProvidersEnabledError()

        verify(view).setNoResultsTextViewVisible(true)
        verify(view).setRefreshing(false)
        verify(view).showNoProvidersError()
    }

    @Test
    fun testNoPermissionsError() {
        val presenter = StationPassListPresenterImpl(mockRepository, mockScheduler, true)
        val view : StationPassListView = mock()
        presenter.attachView(view)
        presenter.noPermissionsError()
        verify(view).setNoResultsTextViewVisible(true)
        verify(view).setRefreshing(false)
        verify(view).showNoPermissionsError()

    }

}