package com.charliechristensen.spacestation.spacestationpasses.ui.stationPassList;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.charliechristensen.spacestation.spacestationpasses.R;
import com.charliechristensen.spacestation.spacestationpasses.data.models.PassInfo;
import com.charliechristensen.spacestation.spacestationpasses.ui.BaseMvpActivity;

import org.jetbrains.annotations.Nullable;

import java.util.List;

import butterknife.BindView;

/**
 * Activity to display the list of passes for given coordinates
 *
 * If location monitoring is enabled it will attempt to get your location
 * and will update the list on major location changes. This is helpful
 * to give some data to the user while the location can be narrowed down
 *
 */

public class StationPassActivity extends BaseMvpActivity<StationPassListView, StationPassListPresenter> implements StationPassListView, LocationListener {

    private enum StationPassListViewType{
        SINGLE_COORDINATES,
        MONITOR_LOCATION
    }
    private static final String KEY_VIEW_TYPE = "KEY_VIEW_TYPE";
    private static final String KEY_LATITUDE = "KEY_LATITUDE";
    private static final String KEY_LONGITUDE = "KEY_LONGITUDE";
    private static final double INVALID_PARAMETER = -181;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.noResultsTextView)
    TextView noResultsTextView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private LocationManager locationManager;
    private final StationPassListAdapter adapter = new StationPassListAdapter();

    //region Static Activity Helpers

    public static void startPassListActivity(Context context, double latitude, double longitude){
        Intent activityIntent = new Intent(context, StationPassActivity.class);
        activityIntent.putExtra(KEY_VIEW_TYPE, StationPassListViewType.SINGLE_COORDINATES);
        activityIntent.putExtra(KEY_LATITUDE, latitude);
        activityIntent.putExtra(KEY_LONGITUDE, longitude);
        context.startActivity(activityIntent);
    }

    public static void startPassListActivity(Context context){
        Intent activityIntent = new Intent(context, StationPassActivity.class);
        activityIntent.putExtra(KEY_VIEW_TYPE, StationPassListViewType.MONITOR_LOCATION);
        context.startActivity(activityIntent);
    }

    //endregion

    //region Lifecycle

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.space_station_flyovers));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.refreshList());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        recyclerView.setAdapter(null);
    }

    //endregion

    //region BaseMvpActivity

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_station_pass_list;
    }

    @NonNull
    @Override
    public StationPassListPresenter createPresenter() {
        StationPassListViewType viewType = (StationPassListViewType) getIntent().getSerializableExtra(KEY_VIEW_TYPE);
        if(viewType == null){
            throw new IllegalStateException("Must pass a StationPassListViewType when creating StationPassActivity");
        }
        if(viewType == StationPassListViewType.MONITOR_LOCATION){
            return new StationPassListPresenterImpl(getRepository(), getScheduler(), true);
        }else{
            double latitude = getIntent().getDoubleExtra(KEY_LATITUDE, INVALID_PARAMETER);
            double longitude = getIntent().getDoubleExtra(KEY_LONGITUDE, INVALID_PARAMETER);
            return new StationPassListPresenterImpl(
                    getRepository(),
                    getScheduler(),
                    false,
                    latitude,
                    longitude);
        }
    }

    //endregion

    //region Options Menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_refresh){
            presenter.refreshList();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //endregion

    //region StationPassListView

    @Override
    public void addItems(List<PassInfo> objects, boolean refresh){
        if(refresh){
            adapter.clearItems();
        }
        adapter.addItems(objects);
    }

    @Override
    public void setNoResultsTextViewVisible(boolean visible){
        if(visible){
            noResultsTextView.setVisibility(View.VISIBLE);
        }else{
            noResultsTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void setRefreshing(Boolean refreshing){
        swipeRefreshLayout.setRefreshing(refreshing);
    }

    //endregion

    //region Location Monitoring

    @Override
    public void startMonitoringLocation() {
        if(locationManager == null) {
            locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        }
        //Check if we have been granted permissions and if we are able to obtain a location manager
        //If not, we notify the user
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || locationManager == null) {
            presenter.noPermissionsError();
            return;
        }
        //We want to see if we are able to use the GPS
        //If not we will attempt to use the network for location
        //If we are unable to access either we notify the user
        String provider;
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            provider = LocationManager.GPS_PROVIDER;
        }else if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
            provider = LocationManager.NETWORK_PROVIDER;
        }else{
            presenter.noProvidersEnabledError();
            return;
        }
        //Here we check to see if we have a last known location so we can
        //get some data to the user as fast as possible
        Location location = locationManager.getLastKnownLocation(provider);
        if(location != null){
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            presenter.locationUpdated(latitude, longitude);
        }
        //Subscribe to location updates
        //These  values could be tweaked find a balance between efficiency and accuracy
        locationManager.requestLocationUpdates(provider, 0, 0, this);
    }

    @Override
    public void stopMonitoringLocation() {
        if(locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    //endregion

    //region Error Reporting

    @Override
    public void showNoPermissionsError() {
        showToast("Error: Location permissions must be allowed");
    }

    @Override
    public void showNoProvidersError() {
        showToast("Error: Cannot determine location based on hardware");
    }

    @Override
    public void showNetworkError() {
        showToast("Error connecting to the network");
    }

    @Override
    public void showUnknownError() {
        showToast("An unknown error occurred, try again later");
    }

    @Override
    public void showInvalidCoordinatesError() {
        showToast("InvalidCoordinates");
    }

    //endregion

    //region LocationListener

    @Override
    public void onLocationChanged(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        presenter.locationUpdated(latitude, longitude);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) { }

    @Override
    public void onProviderEnabled(String s) { }

    @Override
    public void onProviderDisabled(String s) { }

    //endregion

}
