package com.charliechristensen.spacestation.spacestationpasses.ui;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Abstract presenter with some basic functionality
 *
 * Holds a CompositeDisposable that can be added to
 * which will dispose when the view is detached
 *
 */

public abstract class BaseMvpPresenter<V extends MvpView> extends MvpBasePresenter<V> implements MvpPresenter<V> {


    protected final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    public void onViewBound(V view) {}

    @Override
    public void detachView() {
        super.detachView();
        disposables.clear();
    }
}
