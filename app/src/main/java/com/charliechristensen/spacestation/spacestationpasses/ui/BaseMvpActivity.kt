package com.charliechristensen.spacestation.spacestationpasses.ui

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.widget.Toast
import butterknife.ButterKnife
import com.charliechristensen.spacestation.spacestationpasses.MainApplication
import com.charliechristensen.spacestation.spacestationpasses.data.repository.Repository
import com.charliechristensen.spacestation.spacestationpasses.util.scheduler.RxScheduler
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import com.hannesdorfmann.mosby3.mvp.MvpView

/**
 * Abstract Activity which provides some basic functionality and helper methods
 */

abstract class BaseMvpActivity<V : MvpView, P : MvpPresenter<V>> : MvpActivity<V, P>() {

    private val mainApplication: MainApplication by lazy {
        application as MainApplication
    }

    protected val repository: Repository
        get() = mainApplication.repository

    protected val scheduler: RxScheduler
        get() = mainApplication.scheduler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResource())
        ButterKnife.bind(this)
        presenter.onViewBound(mvpView)
    }

    @LayoutRes
    protected abstract fun getLayoutResource(): Int

    protected fun showToast(message: String){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    protected fun showToast(resId: Int){
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show()
    }

}
