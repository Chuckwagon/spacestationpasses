package com.charliechristensen.spacestation.spacestationpasses.data.cache;

import android.support.annotation.NonNull;
import android.support.v4.util.LruCache;

import com.charliechristensen.spacestation.spacestationpasses.data.models.CacheCoordinate;
import com.charliechristensen.spacestation.spacestationpasses.data.models.StationPass;

import io.reactivex.Single;

/**
 * Implementation for the Cache
 */

public class CacheImpl implements Cache {

    private final LruCache<CacheCoordinate, StationPass> cache;

    public CacheImpl(){
        super();
        int cacheSize = 1024 * 512; // 512kiB would ideally be tweaked
        cache = new LruCache<>(cacheSize);
    }

    @Override
    public Single<Boolean> getHasCacheForCoordinate(double latitude, double longitude){
        CacheCoordinate coordinate = new CacheCoordinate(latitude, longitude);
        return Single.just(cache.get(coordinate) != null);
    }

    @Override
    public Single<StationPass> getPassInfoForCoordinate(double latitude, double longitude){
        CacheCoordinate coordinate = new CacheCoordinate(latitude, longitude);
        StationPass stationPass = cache.get(coordinate);
        if(stationPass == null){
            return Single.error(new CacheMissException());
        }
        return Single.just(stationPass);
    }

    @Override
    public StationPass setPassInfoForCoordinate(@NonNull StationPass stationPass, double latitude, double longitude){
        synchronized (cache){
            CacheCoordinate coordinate = new CacheCoordinate(latitude, longitude);
            return cache.put(coordinate, stationPass);
        }
    }

}
