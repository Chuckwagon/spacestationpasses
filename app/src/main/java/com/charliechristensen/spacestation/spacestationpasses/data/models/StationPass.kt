package com.charliechristensen.spacestation.spacestationpasses.data.models

import com.google.gson.annotations.SerializedName

/**
 * Data model for mapping the station pass responses from the api server
 */
data class StationPass(val message: String,
                       val request: PassRequest,
                       @SerializedName("response") val passData: List<PassInfo>)