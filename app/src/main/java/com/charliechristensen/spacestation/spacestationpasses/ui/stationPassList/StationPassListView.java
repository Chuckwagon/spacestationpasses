package com.charliechristensen.spacestation.spacestationpasses.ui.stationPassList;

import com.charliechristensen.spacestation.spacestationpasses.data.models.PassInfo;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

/**
 * View interface for the station pass list
 */

public interface StationPassListView extends MvpView {

    void addItems(List<PassInfo> objects, boolean refresh);
    void setNoResultsTextViewVisible(boolean visible);

    void startMonitoringLocation();
    void stopMonitoringLocation();

    void setRefreshing(Boolean refreshing);

    void showNoPermissionsError();
    void showNoProvidersError();
    void showNetworkError();
    void showUnknownError();
    void showInvalidCoordinatesError();
}
