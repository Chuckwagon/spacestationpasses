package com.charliechristensen.spacestation.spacestationpasses.ui.mainScreen;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.charliechristensen.spacestation.spacestationpasses.R;
import com.charliechristensen.spacestation.spacestationpasses.ui.BaseMvpActivity;
import com.charliechristensen.spacestation.spacestationpasses.ui.stationPassList.StationPassActivity;

import org.jetbrains.annotations.Nullable;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Activity class for the Home Screen.
 *
 * This screen allows the user to input custom
 * coordinates to get a list of Station Passes.
 *
 * This also handles the permissions for getting
 * the user's location.
 *
 * Once the list type is chosen the proper arguments
 * are passed to the next activity to create the list.
 *
 */

public class HomeActivity extends BaseMvpActivity<HomeView, HomePresenter> implements HomeView {

    private static final int PERMISSION_ACCESS_LOCATION = 767;

    @BindView(R.id.latitudeTextInputLayout)
    TextInputLayout latitudeTextInputLayout;
    @BindView(R.id.latitudeEditText)
    TextInputEditText latitudeEditText;
    @BindView(R.id.longitudeEditText)
    TextInputEditText longitudeEditText;
    @BindView(R.id.longitudeTextInputLayout)
    TextInputLayout longitudeTextInputLayout;

    //region BaseMvpActivity


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.home));
    }

    @Override
    public int getLayoutResource() {
        return R.layout.activity_home;
    }

    @NonNull
    @Override
    public HomePresenter createPresenter() {
        return new HomePresenterImpl();
    }

    //endregion

    //region Click Handlers

    @OnClick(R.id.getPassesWithCoordinatesButton)
    public void getPassesWithCoordinatesButtonClick(){
        String latitude = latitudeEditText.getText().toString();
        String longitude = longitudeEditText.getText().toString();
        presenter.getStationPassesWithCoordinatesButtonClick(latitude, longitude);
    }

    @OnClick(R.id.getPassesWithCurrentLocationButton)
    public void getPassesWithCurrentLocationButtonClick(){
        presenter.getStationPassesWithCurrentLocationButtonClick();
    }

    //endregion

    //region Error Messages

    @Override
    public void showLatitudeError() {
        latitudeTextInputLayout.setError(getString(R.string.must_be_between_90));
    }

    @Override
    public void hideLatitudeError() {
        latitudeTextInputLayout.setError(null);
    }

    @Override
    public void showLongitudeError() {
        longitudeTextInputLayout.setError(getString(R.string.must_be_between_180));
    }

    @Override
    public void hideLongitudeError() {
        longitudeTextInputLayout.setError(null);
    }

    @Override
    public void showPermissionDeniedError() {
        showToast(R.string.must_accept_permissions);
    }

    //endregion

    //region Push Views

    @Override
    public void pushStationPassListActivity(double latitude, double longitude){
        StationPassActivity.startPassListActivity(this, latitude, longitude);
    }

    @Override
    public void pushStationPassListActivityWithLocationMonitoring(){
        StationPassActivity.startPassListActivity(this);
    }

    //endregion

    //region Permissions

    @Override
    public boolean getHasLocationPermissions(){
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void requestLocationPermissions(){
        ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_ACCESS_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_ACCESS_LOCATION) {
            if (permissions.length == 1
                    && permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                presenter.locationPermissionsGranted();
            }else{
                presenter.locationPermissionDenied();
            }
        }
    }

    //endregion

}
