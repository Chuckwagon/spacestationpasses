package com.charliechristensen.spacestation.spacestationpasses.data.models

/**
 * Data model for mapping the request field for the StationPass model
 */
data class PassRequest(val altitude: Int,
                       val datetime: Long,
                       val latitude: Double,
                       val longitude: Double,
                       val passes: Int)