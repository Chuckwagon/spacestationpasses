package com.charliechristensen.spacestation.spacestationpasses.util

/**
 * Created by Chuff on 12/16/17.
 */

object CoordinateUtil {

    const val MAX_LATITUDE = 90.0
    const val MAX_LONGITUDE = 180.0

    fun areValidCoordinates(latitude: Double, longitude: Double): Boolean{
        return isLatitudeValid(latitude) && isLongitudeValid(longitude)
    }

    fun isLatitudeValid(latitude: Double): Boolean {
        return latitude in -MAX_LATITUDE..MAX_LATITUDE
    }

    fun isLongitudeValid(longitude: Double): Boolean{
        return longitude in -MAX_LONGITUDE..MAX_LONGITUDE
    }
}
