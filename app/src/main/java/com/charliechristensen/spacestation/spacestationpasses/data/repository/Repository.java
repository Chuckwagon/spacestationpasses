package com.charliechristensen.spacestation.spacestationpasses.data.repository;

import android.support.annotation.NonNull;

import com.charliechristensen.spacestation.spacestationpasses.data.models.PassInfo;

import java.util.List;

import io.reactivex.Single;

/**
 * Interface for the RRRepository
 *
 *
 * Provides the app with data while deciding if it should pull data from
 * the network or from the cache.
 * This hides the implementation details so other parts of the app don't
 * have to worry about where the data comes from, as data is provided.
 *
 *
 */

public interface Repository {
    @NonNull
    Single<List<PassInfo>> getPassTimesForCoordinates(double latitude, double longitude, int count);
}
