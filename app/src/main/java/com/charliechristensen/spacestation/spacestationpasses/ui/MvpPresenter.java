package com.charliechristensen.spacestation.spacestationpasses.ui;

import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Base presenter interface for mvp presenters
 */

public interface MvpPresenter<V extends MvpView> extends com.hannesdorfmann.mosby3.mvp.MvpPresenter<V> {
    /**
     * This is called when Butterknife has bound views so it is safe to make calls on view components
     */
    void onViewBound(V view);
}
