package com.charliechristensen.spacestation.spacestationpasses.data.cache;

import com.charliechristensen.spacestation.spacestationpasses.data.models.StationPass;

import io.reactivex.Single;

/**
 * Interface for the Cache
 *
 * Caches data for a given coordinate. In a production
 * situation, we would maybe want to have a tolerance so
 * we don't make unnecessary network requests. One solution
 * could be implemented by overriding equals() in the
 * CacheCoordinate class. Also could look into expiring
 * entries after a given period of time.
 *
 */

public interface Cache {
    Single<Boolean> getHasCacheForCoordinate(double latitude, double longitude);
    Single<StationPass> getPassInfoForCoordinate(double latitude, double longitude);
    StationPass setPassInfoForCoordinate(StationPass stationPass, double latitude, double longitude);
}
