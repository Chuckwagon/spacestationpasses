package com.charliechristensen.spacestation.spacestationpasses.ui.mainScreen;

import com.charliechristensen.spacestation.spacestationpasses.ui.BaseMvpPresenter;
import com.charliechristensen.spacestation.spacestationpasses.util.CoordinateUtil;

/**
 * Presenter interface for the home screen
 */

public class HomePresenterImpl extends BaseMvpPresenter<HomeView> implements HomePresenter {

    //region Button Clicks

    @Override
    public void getStationPassesWithCoordinatesButtonClick(String latitudeString, String longitudeString){
        parseFieldsAndPushListView(latitudeString, longitudeString);
    }

    @Override
    public void getStationPassesWithCurrentLocationButtonClick(){
        ifViewAttached(view -> {
            if(view.getHasLocationPermissions()){
                view.pushStationPassListActivityWithLocationMonitoring();
            }else{
                view.requestLocationPermissions();
            }
        });
    }

    //endregion

    //region Field Parsing

    private void parseFieldsAndPushListView(String latitudeString, String longitudeString){
        ifViewAttached(view -> {
            double latitudeDouble = 0;
            double longitudeDouble = 0;
            boolean inputIsValid = true;
            //Try to parse the Strings into doubles, if we encounter errors we will report them
            //to the corresponding EditText
            try {
                latitudeDouble = Double.parseDouble(latitudeString);
                if (CoordinateUtil.INSTANCE.isLatitudeValid(latitudeDouble)) {
                    view.hideLatitudeError();
                } else {
                    inputIsValid = false;
                    view.showLatitudeError();
                }
            }catch (NumberFormatException e){
                view.showLatitudeError();
                inputIsValid = false;
            }
            try {
                longitudeDouble = Double.parseDouble(longitudeString);
                if (CoordinateUtil.INSTANCE.isLongitudeValid(longitudeDouble)) {
                    view.hideLongitudeError();
                } else {
                    inputIsValid = false;
                    view.showLongitudeError();
                }
            }catch (NumberFormatException e){
                view.showLongitudeError();
                inputIsValid = false;
            }
            //If the inputs are valid we pass them to the list activity
            if(inputIsValid){
                view.pushStationPassListActivity(latitudeDouble, longitudeDouble);
            }
        });
    }

    //endregion

    //region Permissions

    @Override
    public void locationPermissionsGranted() {
        ifViewAttached(HomeView::pushStationPassListActivityWithLocationMonitoring);
    }

    @Override
    public void locationPermissionDenied() {
        ifViewAttached(HomeView::showPermissionDeniedError);
    }

    //endregion

}
